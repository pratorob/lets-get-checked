package ro.berto.prato.challenge

import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import ro.berto.prato.challenge.api.ApiClient
import ro.berto.prato.challenge.repo.CommentRepository
import ro.berto.prato.challenge.repo.PostRepository
import ro.berto.prato.challenge.room.AppDatabase
import java.util.concurrent.Executor
import java.util.concurrent.Executors

private class UiExecutor: Executor {
    private val mainThreadHandler = Handler(Looper.getMainLooper())
    override fun execute(command: Runnable) {
        mainThreadHandler.post(command)
    }
}

class ApplicationServices(val app: Application) {

    companion object {
        private val LOCK = Any()
        private var instance: ApplicationServices? = null
        fun instance(context: Context): ApplicationServices {
            synchronized(LOCK) {
                if (instance == null) {
                    instance = ApplicationServices(
                        app = context.applicationContext as Application)
                }
                return instance!!
            }
        }
    }

    public val db by lazy {
        AppDatabase.getInstance(app)
    }

    public val api by lazy {
        ApiClient.getInstance(app)
    }

    private val postRepo by lazy {
        PostRepository(this)
    }

    private val commentRepo by lazy {
        CommentRepository(this)
    }

    private val uiExecutor by lazy {
        UiExecutor()
    }

    private val backgroundExecutor by lazy {
        Executors.newFixedThreadPool(2)
    }



    fun getPostRepository() = postRepo

    fun getCommentRepository() = commentRepo

    fun executeOnUI(block:() -> Unit) {
        uiExecutor.execute(runnable(block))
    }

    fun executeOnBKG(block:() -> Unit) {
        backgroundExecutor.execute(runnable(block))
    }

}
fun runnable(f: () -> Unit): Runnable = object: Runnable {override fun run() {f()}}

fun Context.services(): ApplicationServices {
    return ApplicationServices.instance(applicationContext)
}