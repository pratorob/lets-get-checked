package ro.berto.prato.challenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ro.berto.prato.challenge.repo.CommentRepository
import ro.berto.prato.challenge.repo.DataLoader
import ro.berto.prato.challenge.repo.PostRepository
import ro.berto.prato.challenge.room.Post

class CommentsViewModel(val repo: CommentRepository): ViewModel() {

    fun getComments(post: Int) = repo.loadComments(post)

    fun postComment(post: Int, comment:String) = repo.postComment(post, comment)
}