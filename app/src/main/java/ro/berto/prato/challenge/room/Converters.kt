package ro.berto.prato.challenge.room

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class Converters {

    companion object {
        val apiDateFormat: SimpleDateFormat =
            SimpleDateFormat("yyyy-MM-dd")
    }

    @TypeConverter fun stringToDate(date: String): Date =
        apiDateFormat.parse(date)

    @TypeConverter fun dateToString(value: Date): String  =
        apiDateFormat.format(value)

}