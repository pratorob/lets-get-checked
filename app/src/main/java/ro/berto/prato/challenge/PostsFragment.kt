package ro.berto.prato.challenge


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.posts_fragment.*
import ro.berto.prato.challenge.repo.DataLoadingStatus
import ro.berto.prato.challenge.room.Post
import ro.berto.prato.challenge.viewmodel.PostsViewModel
import ro.berto.prato.challenge.viewmodel.SelectedPostViewModel
import ro.berto.prato.challenge.viewmodel.ViewModelsFactory


class PostsFragment : Fragment() {

    companion object {
        const val TAG = "ro.berto.prato.challenge.PostsFragment"
    }

    private lateinit var viewModel: PostsViewModel
    private lateinit var adapter: PostsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.posts_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this,
            ViewModelsFactory(context!!))
            .get(PostsViewModel::class.java)


        adapter = PostsAdapter(object: PostsAdapter.OnPostClickListener{
            override fun onPostClick(post: Post) {

                val selectedPostViewModel = ViewModelProviders.of(activity!!,
                    SelectedPostViewModel.Factory())
                    .get(SelectedPostViewModel::class.java)

                selectedPostViewModel.setSelectedPost(post)
            }
        })
        posts_list.adapter = adapter

        viewModel.getPosts(1).observe( this, Observer { result ->

            if(result.loadingStatus == DataLoadingStatus.Status.LOADING)
                loading_progress.visibility = View.VISIBLE
            else
                loading_progress.visibility = View.GONE

            if(result.data != null) {
                adapter.setPosts(result.data)
            }
        })

    }

}
