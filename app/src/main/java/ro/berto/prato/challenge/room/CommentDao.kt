package ro.berto.prato.challenge.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface CommentDao {

    @Query("SELECT * FROM comments WHERE postId = :postId ORDER BY date ASC")
    fun load(postId: Int): List<Comment>

    @Query("SELECT * FROM comments WHERE id = :commentId LIMIT 1")
    fun loadComment(commentId: Int): Comment

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: List<Comment>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: Comment)
}