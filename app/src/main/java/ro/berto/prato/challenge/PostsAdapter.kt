package ro.berto.prato.challenge

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ro.berto.prato.challenge.room.Post

class PostsAdapter(val clickListener: OnPostClickListener): RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    interface OnPostClickListener {
        fun onPostClick(post: Post)
    }

    private var posts: List<Post> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_post, parent, false)

        return PostViewHolder(view)

    }

    override fun getItemCount(): Int {
       return posts.size
    }

    fun setPosts(posts: List<Post>) {
        this.posts = posts
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {

        val post = posts[position]

        holder.textViewTitle.text = post.title
        holder.textViewDescription.text = post.description
        holder.textViewAuthor.text =  String.format("by %S", post.author)

        holder.view.setOnClickListener {
            clickListener.onPostClick(post)
        }

    }

    class PostViewHolder(val view:View) : RecyclerView.ViewHolder(view) {

        val textViewTitle by lazy {
            view.findViewById(R.id.textViewTitle) as TextView
        }

        val textViewDescription by lazy {
            view.findViewById(R.id.textViewDescription) as TextView
        }

        val textViewAuthor by lazy {
            view.findViewById(R.id.textViewAuthor) as TextView
        }

    }
}