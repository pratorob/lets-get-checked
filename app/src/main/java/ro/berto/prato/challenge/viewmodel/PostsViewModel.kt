package ro.berto.prato.challenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ro.berto.prato.challenge.repo.DataLoader
import ro.berto.prato.challenge.repo.PostRepository
import ro.berto.prato.challenge.room.Post

class PostsViewModel(val repo: PostRepository): ViewModel() {

    fun getPosts(page: Int) = repo.loadPosts(page)
}