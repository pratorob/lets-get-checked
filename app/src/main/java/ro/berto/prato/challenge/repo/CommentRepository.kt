package ro.berto.prato.challenge.repo

import androidx.lifecycle.LiveData
import ro.berto.prato.challenge.ApplicationServices
import ro.berto.prato.challenge.room.Comment
import java.util.*

class CommentRepository(private val applicationServices: ApplicationServices) {

    val commentDao = applicationServices.db.commentDao()


     fun loadComments(postId: Int): LiveData<DataLoadingStatus<List<Comment>>> {

         return object: DataLoader<List<Comment>>(applicationServices) {

             override fun storeDataInDb(data: List<Comment>) {
                 commentDao.insert(data)
             }

             override fun loadDataFromNetwork() = applicationServices.api.getPostComments(postId)

             override fun loadDataFromDb(): List<Comment> = commentDao.load(postId)


         }.asLiveData()
     }

    fun postComment(postId: Int, comment:String): LiveData<DataLoadingStatus<Comment>> {

        var newComment = Comment(null, postId,
            null,"Roberto", Date(), comment)

        return object: DataLoader<Comment>(applicationServices) {

            override fun storeDataInDb(data: Comment) {
                commentDao.insert(data)
                newComment = data
            }

            override fun loadDataFromNetwork() = applicationServices.api.sendPostComment(postId, newComment)

            override fun loadDataFromDb(): Comment = newComment


        }.asLiveData()
    }

}