package ro.berto.prato.challenge.api

import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.http.*
import ro.berto.prato.challenge.room.Comment
import ro.berto.prato.challenge.room.Post

interface ApiService {

    companion object {
        const val PAGE_SIZE = 10
    }

    @GET("posts")
    fun getPosts(@Query("_page") page: Int = 1,
                 @Query("_limit") pageSize: Int = PAGE_SIZE): Call<List<Post>>

    @GET("posts/{post_id}")
    fun getPost(@Path("post_id") postId: Int = 1): LiveData<Post>

    @GET("posts/{post_id}/comments")
    fun getPostComments(@Path("post_id") postId: Int = 1): Call<List<Comment>>

    @POST("posts/{post_id}/comments")
    fun sendPostComment(@Path("post_id")postId: Int,@Body comment: Comment): Call<Comment>

}