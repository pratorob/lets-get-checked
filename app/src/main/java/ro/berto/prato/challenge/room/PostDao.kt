package ro.berto.prato.challenge.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface PostDao {

    @Query("SELECT * FROM posts ORDER BY publish_date")
    fun load(): List<Post>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: List<Post>)

}