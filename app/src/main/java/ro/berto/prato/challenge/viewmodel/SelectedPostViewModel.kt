package ro.berto.prato.challenge.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ro.berto.prato.challenge.room.Post


class SelectedPostViewModel: ViewModel() {

     class Factory: ViewModelProvider.Factory {

        companion object {
            val model = SelectedPostViewModel()
        }

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {

            return Factory.model as T

        }


    }

    private val selectedPost = MutableLiveData<Post>()

    fun setSelectedPost(post: Post){
        selectedPost.value = post
    }

    fun getAsLiveData() = selectedPost
}