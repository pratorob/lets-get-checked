package ro.berto.prato.challenge.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ro.berto.prato.challenge.ApplicationServices


data class DataLoadingStatus<DataType>(val loadingStatus: Status, val data: DataType?) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <DataType> success(data: DataType?): DataLoadingStatus<DataType> {
            return DataLoadingStatus(Status.SUCCESS, data)
        }

        fun <DataType> error(): DataLoadingStatus<DataType> {
            return DataLoadingStatus(Status.ERROR, null)
        }

        fun <DataType> loading(): DataLoadingStatus<DataType> {
            return DataLoadingStatus(Status.LOADING, null)
        }
    }

}

abstract class DataLoader<DataType>(val appServices: ApplicationServices) {

    private val data = MutableLiveData<DataLoadingStatus<DataType>>()

    abstract fun storeDataInDb(data:DataType)

     abstract fun loadDataFromNetwork(): Call<DataType>

     abstract fun loadDataFromDb(): DataType

    init {
        loadData()
    }

     fun loadData() {

         data.value = DataLoadingStatus.loading()

         appServices.executeOnBKG {

             val cached = loadDataFromDb()

             appServices.executeOnUI {
                 data.value = DataLoadingStatus.success(cached)
             }

             val call = loadDataFromNetwork()

             call.enqueue(object : Callback<DataType>{
                 override fun onFailure(call: Call<DataType>, t: Throwable) {
                     data.value = DataLoadingStatus.error()
                 }

                 override fun onResponse(call: Call<DataType>, response: Response<DataType>) {

                     appServices.executeOnBKG {

                         response.body()?.let {
                             storeDataInDb(it)
                         }

                         val cached = loadDataFromDb()
                         appServices.executeOnUI {
                             data.value = DataLoadingStatus.success(cached)
                         }
                     }


            } })
         }
     }

    fun asLiveData() = data as LiveData<DataLoadingStatus<DataType>>

}