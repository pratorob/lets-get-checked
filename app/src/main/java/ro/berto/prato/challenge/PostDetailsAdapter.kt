package ro.berto.prato.challenge

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ro.berto.prato.challenge.room.Comment
import ro.berto.prato.challenge.room.Post

class PostDetailsAdapter(val post: Post): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val ITEM_TYPE_CONTENT = 1
        private const val ITEM_TYPE_COMMENT = 2
    }


    data class ListItem<out T>(val type:Int, val data:T)

    private var items: ArrayList<ListItem<*>> = ArrayList()

    init {
        setComments(ArrayList())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if(viewType == ITEM_TYPE_CONTENT) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_post_content, parent, false)

            return PostContentViewHolder(view)
        }

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_post_comment, parent, false)

        return PostCommentViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return item.type
    }

    override fun getItemCount(): Int {
       return items.size
    }

    fun setComments(comments: List<Comment>) {

        items.clear()
        items.add(ListItem(ITEM_TYPE_CONTENT, post))

        for(comment in comments) {
            items.add(ListItem(ITEM_TYPE_COMMENT, comment))
        }

        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = items[position]

        if(item.type == ITEM_TYPE_CONTENT) {
            val h = holder as PostContentViewHolder
            val p = item.data as Post
            h.textViewDescription.text = Html.fromHtml(p.content,0)
            h.textViewAuthor.text =  String.format("by %S", p.author)
        }
        else if(item.type == ITEM_TYPE_COMMENT) {
            val h = holder as PostCommentViewHolder
            val c = item.data as Comment
            h.textViewComment.text = Html.fromHtml(c.content,0)
            h.textViewAuthor.text =  String.format("%s said:", c.user)
        }

    }

    class PostContentViewHolder(val view:View) : RecyclerView.ViewHolder(view) {

        val textViewDescription by lazy {
            view.findViewById(R.id.textViewContent) as TextView
        }

        val textViewAuthor by lazy {
            view.findViewById(R.id.textViewAuthor) as TextView
        }

    }

    class PostCommentViewHolder(val view:View) : RecyclerView.ViewHolder(view) {

        val textViewComment by lazy {
            view.findViewById(R.id.textViewComment) as TextView
        }

        val textViewAuthor by lazy {
            view.findViewById(R.id.textViewAuthor) as TextView
        }

    }
}