package ro.berto.prato.challenge.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ro.berto.prato.challenge.services


class ViewModelsFactory(val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        val services = context.services()

        if(modelClass.isAssignableFrom(PostsViewModel::class.java)){
            return PostsViewModel(services.getPostRepository()) as T
        }

        if(modelClass.isAssignableFrom(CommentsViewModel::class.java)){
            return CommentsViewModel(services.getCommentRepository()) as T
        }



        throw Exception()
    }
}