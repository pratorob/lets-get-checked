package ro.berto.prato.challenge.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "comments", foreignKeys = arrayOf(ForeignKey(
    entity = Post::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("postId")),
    ForeignKey(entity = Comment::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("parent_id"))))
data class Comment(@PrimaryKey val id: Int?,
                   val postId: Int,
                   val parent_id: Int?,
                   val user: String,
                   val date: Date,
                   val content: String  )