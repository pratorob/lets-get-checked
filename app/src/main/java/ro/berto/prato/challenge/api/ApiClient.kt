package ro.berto.prato.challenge.api

import android.content.Context
import androidx.lifecycle.LiveData
import retrofit2.Call
import ro.berto.prato.challenge.room.AppDatabase
import ro.berto.prato.challenge.room.Comment
import ro.berto.prato.challenge.room.Post
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ro.berto.prato.challenge.R


class ApiClient(val baseApiUrl: String): ApiService  {

    override fun sendPostComment(postId: Int, comment: Comment): Call<Comment> {
        return apiImplementation.sendPostComment(postId, comment)
    }

    override fun getPosts(page: Int, pageSize: Int): Call<List<Post>> {
        return apiImplementation.getPosts(page, pageSize)
    }

    override fun getPost(postId: Int): LiveData<Post> {
        return apiImplementation.getPost(postId)
    }

    override fun getPostComments(postId: Int): Call<List<Comment>> {
      return apiImplementation.getPostComments(postId)
    }

    private val apiImplementation: ApiService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseApiUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiImplementation = retrofit.create(ApiService::class.java)
    }

    companion object {

        private var instance: ApiClient? = null

        fun getInstance(context: Context): ApiClient {
            return instance ?: synchronized(this) {
                 val baseApiURl = context.getString(R.string.api_base_url)
                instance ?: ApiClient(baseApiURl).also { instance = it }
            }
        }
    }

}

