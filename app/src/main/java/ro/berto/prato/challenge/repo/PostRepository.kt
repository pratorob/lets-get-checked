package ro.berto.prato.challenge.repo

import androidx.lifecycle.LiveData
import ro.berto.prato.challenge.ApplicationServices
import ro.berto.prato.challenge.api.ApiClient
import ro.berto.prato.challenge.api.ApiService
import ro.berto.prato.challenge.room.Post
import ro.berto.prato.challenge.room.PostDao

class PostRepository(private val applicationServices: ApplicationServices) {

    val postDao = applicationServices.db.postDao()


     fun loadPosts(nextPage: Int = 1): LiveData<DataLoadingStatus<List<Post>>> {

         return object: DataLoader<List<Post>>(applicationServices) {

             override fun storeDataInDb(data: List<Post>) {
                 postDao.insert(data)
             }

             override fun loadDataFromNetwork() = applicationServices.api.getPosts(nextPage)

             override fun loadDataFromDb(): List<Post> = postDao.load()


         }.asLiveData()
     }

}