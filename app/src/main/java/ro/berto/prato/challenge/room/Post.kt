package ro.berto.prato.challenge.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "posts")
data class Post(@PrimaryKey var id: Int,
                var title: String,
                var author: String,
                var publish_date: Date,
                var description: String,
                var content: String)