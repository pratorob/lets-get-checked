package ro.berto.prato.challenge


import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.post_details_fragment.*
import kotlinx.android.synthetic.main.posts_fragment.*
import ro.berto.prato.challenge.repo.DataLoadingStatus
import ro.berto.prato.challenge.room.Post
import ro.berto.prato.challenge.viewmodel.CommentsViewModel
import ro.berto.prato.challenge.viewmodel.PostsViewModel
import ro.berto.prato.challenge.viewmodel.SelectedPostViewModel
import ro.berto.prato.challenge.viewmodel.ViewModelsFactory


class PostDetailsFragment : Fragment() {



    private lateinit var adapter: PostDetailsAdapter
    private lateinit var viewModel: CommentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val selectedPostViewModel = ViewModelProviders.of(this,
            SelectedPostViewModel.Factory())
            .get(SelectedPostViewModel::class.java)

        val post = selectedPostViewModel.getAsLiveData().value!!

        textViewTitle.text = post.title

        adapter = PostDetailsAdapter(post)
        post_details.adapter = adapter

        viewModel = ViewModelProviders.of(this,
            ViewModelsFactory(context!!))
            .get(CommentsViewModel::class.java)

        loadComments(post)

        sendComment.setOnClickListener {

           val comment:String =  editTextViewComment.text.toString()

           if(!TextUtils.isEmpty(comment)) {
               sendComment.isEnabled = false
               viewModel.postComment(post.id, comment).observe(this, Observer {result ->

                   if(result.loadingStatus != DataLoadingStatus.Status.LOADING) {
                       sendComment.isEnabled = true
                   }
                   if(result.loadingStatus == DataLoadingStatus.Status.SUCCESS) {
                       editTextViewComment.text.clear()
                       loadComments(post)
                   }
               })
           }

        }
    }

    fun loadComments(post: Post) {
        viewModel.getComments(post.id).observe( this, Observer { result ->

            if(result.loadingStatus != DataLoadingStatus.Status.LOADING) {
                if(result.data != null && !result.data.isEmpty()) {
                    adapter.setComments(result.data)
                }else {
                    Toast.makeText(context,
                        "No Comments for this post",
                        Toast.LENGTH_SHORT ).show()
                }
            }
        })
    }

}
