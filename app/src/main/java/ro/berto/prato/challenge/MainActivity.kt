package ro.berto.prato.challenge

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import kotlinx.android.synthetic.main.activity_main.*
import ro.berto.prato.challenge.viewmodel.SelectedPostViewModel


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        val selectedPostViewModel = ViewModelProviders.of(this,
            SelectedPostViewModel.Factory())
            .get(SelectedPostViewModel::class.java)

        val postsFragment:PostsFragment =
            supportFragmentManager.findFragmentByTag(PostsFragment.TAG) as PostsFragment

        selectedPostViewModel.getAsLiveData().observe( postsFragment, Observer { post ->

            val detailsFragment = PostDetailsFragment()

            supportFragmentManager.beginTransaction()
                .add( R.id.container, detailsFragment)
                .addToBackStack(post.title)
                .commit()
        })

    }

    override fun getLifecycle(): Lifecycle {
        return lifecycle
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
