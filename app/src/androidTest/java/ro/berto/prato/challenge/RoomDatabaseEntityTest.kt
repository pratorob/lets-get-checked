package ro.berto.prato.challenge

import android.content.Context
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import ro.berto.prato.challenge.room.AppDatabase
import ro.berto.prato.challenge.room.Post
import ro.berto.prato.challenge.room.PostDao
import java.io.IOException
import java.util.*

@RunWith(AndroidJUnit4::class)
class RoomDatabaseEntityTest {

    private lateinit var postDao: PostDao
    private lateinit var db: AppDatabase

    private val post1 = Post(1,"post 1","post 1 author",Date(),"post 1 des","content")
    private val post2 = Post(2,"post 2","post 2 author",Date(),"post 2 des","content")
    private val post3 = Post(3,"post 3","post 3 author",Date(),"post 3 des","content")



    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        postDao = db.postDao()

        postDao.insert(listOf(post1,post2,post3))
    }


    @Test fun testGetPosts() {
        val list = postDao.load()
        Assert.assertThat(list.size, Matchers.equalTo(3))
    }


    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }


}
